Color Color = new Color();

class Color {
    color red = color(255, 0, 0);
    color green = color(0, 255, 0);
    color blue = color(0, 0, 255);
    color yellow = color(255, 255, 0);
    color grey = color(100, 100, 100);
    color light_grey = color(200, 200, 200);
    color white = color(255, 255, 255);
    color pink = color(240, 120, 230);
    color cyan = color(0, 255, 255);
    color magenta = color(255, 0, 255);
    color black = color(0,0,0);
}  
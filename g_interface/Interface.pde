PImage poli;
Interface Interface = new Interface();


int h(float x) { //1600
  return int(width*(x/1600));
} 
int v(float y) { //900
  return int(height*(y/900));
}

class Interface {
    float tx=0, ty=0, tz=0;
    float target_tx=0, target_ty=0, target_tz=0;
    float rx=0, ry=0, rz=0;
    float target_rx=0, target_ry=0, target_rz=0;
    float grid = 50;
    float scale = 50;

    String mode = "initial"; // initial, visual, drawing, node_options, bar_options.
    String orientation = "";
    boolean mouse_pressed = false;
    Node drawing_node;
    Node options_node;
    Bar options_bar;
    void axis() {
        stroke(Color.green);
        line(0, 0, 0, 100, 0, 0);
        stroke(Color.red);
        line(0, 0, 0, 0, 100, 0);
        stroke(Color.blue);
        line(0, 0, 0, 0, 0, 100);
    }
    void target(Node node) {
        target_tx += width/2 - modelX(node.x,node.y,node.z);
        target_ty += height/2 - modelY(node.x,node.y,node.z);
        target_tz += -modelZ(node.x,node.y,node.z);  
    }
    int triad() {
        int x = mouseX - width/2;
        int y = mouseY - height/2;
        if (x > 0 && y - x/2 < 0) {
            return 3;
        } else if (x<0 && y + x/2 < 0) {
            return 2;
        } else {
            return 1;
        }
    }
    boolean selectPlan(Node node) {
        pushMatrix();
        translate(node.x, node.y, node.z);
        if (triad() == 1) {
            fill(Color.yellow, 50);
            stroke(Color.yellow);
            beginShape();
            vertex(h(40), h(40), 0);
            vertex(h(130), h(40), 0);
            quadraticVertex(h(130), h(130), 0, h(40), h(130), 0);
            endShape(CLOSE);
        } else {
            fill(Color.yellow, 50);
            stroke(Color.yellow);
            beginShape();
            vertex(h(10), h(10), 0);
            vertex(h(100), h(10), 0);
            quadraticVertex(h(100), h(100), 0, h(10), h(100), 0);
            endShape(CLOSE);
        }
        if (triad() == 2) {
            fill(Color.magenta, 50);
            stroke(Color.magenta);
            beginShape();
            vertex(0, h(40), h(40));
            vertex(0, h(130), h(40));
            quadraticVertex(0, h(130), h(130), 0, h(40), h(130));
            endShape(CLOSE);
        }  else {
            fill(Color.magenta, 50);
            stroke(Color.magenta);
            beginShape();
            vertex(0, h(10), h(10));
            vertex(0, h(100), h(10));
            quadraticVertex(0, h(100), h(100), 0, h(10), h(100));
            endShape(CLOSE);
        }
        if (triad() == 3) {
            fill(Color.cyan, 50);
            stroke(Color.cyan);
            beginShape();
            vertex(h(40), 0, h(40));
            vertex(h(130), 0, h(40));
            quadraticVertex(h(130), 0, h(130), h(40), 0, h(130));
            endShape(CLOSE);
        } else {
            fill(Color.cyan, 50);
            stroke(Color.cyan);
            beginShape();
            vertex(h(10), 0, h(10));
            vertex(h(100), 0, h(10));
            quadraticVertex(h(100), 0, h(100), h(10), 0, h(100));
            endShape(CLOSE);
        }
        popMatrix();
        if (triad() == 1) {
            orientation = "xy";
            target_rx = 0;
            target_rz = 0;
        } else if (triad() == 2) {
            orientation = "yz";
            target_rx = PI/2;
            target_rz = PI/2;
        } else {
            orientation = "xz";
            target_rx = PI/2;
            target_rz = 0;
        }
        if (mousePressed) {
            return true;
        } else {
            return false;
        }
        
    }
    void startLine(Node node_r){
        if (mode != "drawing"){
            mode = "drawing";
            drawing_node = node_r;
        } 
    }
    void startNodeOptions(Node node){
        if (mode != "node_options"){
            mode = "node_options";
            options_node = node;
        } 
    }
    PShape circleInside(float cx,float cy,float r){
        // Make a shape
        PShape s = createShape();
        s.beginShape();
        s.noStroke();

        // Exterior part of shape
        s.vertex(0,0);
        s.vertex(0,height);
        s.vertex(width,height);
        s.vertex(width,0);
        s.vertex(0,0);

        // Internal part of shape
        s.beginContour();
        for (float i = 0;i < 2*PI; i+= PI/16 ){
            s.vertex(cx+r*cos(i),cy+r*sin(i));
        }
        s.endContour();

        // Finish off shape
        s.endShape();
        return s;
    }
    void fadedCircle(float cx,float cy,float r){
        PShape s;
        for (float i = 0;i < 1;i+=0.05){
            s = circleInside(cx,cy,r*(1+i));
            color faded_black = color(0,0,0,255*i);
            s.setFill(faded_black);
            shape(s);
        }
    }
    void optionsRect(float i,float f,color c){
        println(str(i)+" "+str(f));
        i = i - floor(i);
        f = f - floor(f);
        if (f >= i){
            float i_,f_,i0,f0;
            noStroke();
            //block 1 ( 0 - .1250)
            i_ = min(max(0.0000,i),0.125);
            f_ = max(min(0.1250,f),0.000);
            i0 = (i_ - 0)/0.125;
            f0 = (f_ - 0)/0.125;
            fill(c);
            rect(h(1550),v(450)+i0*v(270),h(3),v(270)*(f0-i0));
            //curve 1 ( - .1875)
            i_ = min(max(0.1250,i),0.1875);
            f_ = max(min(0.1875,f),0.1250);
            i0 = (i_ - 0.125)/0.0625;
            f0 = (f_ - 0.125)/0.0625;
            fill(c);
            arc(h(1500),v(720),v(100)+h(6),v(100)+h(6),HALF_PI*i0,HALF_PI*f0);
            fill(Color.black);
            arc(h(1500),v(720),v(100),v(100),0,HALF_PI);
            //block 2 ( - .3125 )
            i_ = min(max(0.1875,i),0.3125);
            f_ = max(min(0.3125,f),0.1875);
            i0 = (i_ - 0.1875)/0.125;
            f0 = (f_ - 0.1875)/0.125;
            fill(c);
            rect(h(1250)-v(50)+h(300)*(1-f0),v(770),h(300)*(f0-i0),v(3));
            //curve 2 ( - .3750 )
            i_ = min(max(0.3125,i),0.3750);
            f_ = max(min(0.3750,f),0.3125);
            i0 = (i_ - 0.3125)/0.0625;
            f0 = (f_ - 0.3125)/0.0625;
            fill(c);
            arc(h(1200),v(720),v(100),v(100)+h(6),HALF_PI+HALF_PI*i0,HALF_PI+HALF_PI*f0);
            fill(Color.black);
            arc(h(1200),v(720),v(100)-h(6),v(100),HALF_PI,PI);
            //block 3 ( - .6250 )
            i_ = min(max(0.3750,i),0.6250);
            f_ = max(min(0.6250,f),0.3750);
            i0 = (i_ - 0.375)/0.25;
            f0 = (f_ - 0.375)/0.25;
            fill(c);
            rect(h(1250)-2*v(50),v(180)+v(540)*(1-f0),h(3),v(540)*(f0-i0));
            //curve 3 ( - .6875 )
            i_ = min(max(0.6250,i),0.6875);
            f_ = max(min(0.6875,f),0.6250);
            i0 = (i_ - 0.625)/0.0625;
            f0 = (f_ - 0.625)/0.0625;
            fill(c);
            arc(h(1200),v(180),v(100),v(100),PI+HALF_PI*i0,PI+HALF_PI*f0);
            fill(Color.black);
            arc(h(1200),v(180),v(100)-h(6),v(100)-h(6),PI,PI+HALF_PI);
            //block 4 ( - .8125 )
            i_ = min(max(0.6875,i),0.8125);
            f_ = max(min(0.8125,f),0.6875);
            i0 = (i_ - 0.6875)/0.125;
            f0 = (f_ - 0.6875)/0.125;
            fill(c);
            rect(h(1250)-v(50)+h(300)*i0,v(130),h(300)*(f0-i0),v(3));
            //curve 4 ( - .8750 )
            i_ = min(max(0.8125,i),0.8750);
            f_ = max(min(0.8750,f),0.8125);
            i0 = (i_ - 0.8125)/0.0625;
            f0 = (f_ - 0.8125)/0.0625;
            fill(c);
            arc(h(1500),v(180),v(100)+h(6),v(100),PI+HALF_PI+HALF_PI*i0,PI+HALF_PI+HALF_PI*f0);
            fill(Color.black);
            arc(h(1500),v(180),v(100),v(100)-h(6),PI+HALF_PI,2*PI);
            //block 5 ( - 1.000 )
            i_ = min(max(0.8750,i),1);
            f_ = max(min(1.0000,f),0.875);
            i0 = (i_ - 0.875)/0.125;
            f0 = (f_ - 0.875)/0.125;
            fill(c);
            rect(h(1550),v(180)+v(270)*i0,h(3),v(270)*(f0-i0));
        }
        else{
            optionsRect(i,0.9999999,c);
            optionsRect(0,f,c);
        }
    }
    void nodeOptions(){
        //target(options_node);
        fadedCircle(width/2,height/2,h(150));
        optionsRect((millis()+250)/10000.0,(millis())/10000.0,Color.white);
    }
    void barOptions(){

    }
    void drawLine() {
        float x0 = drawing_node.x;
        float y0 = drawing_node.y;
        float z0 = drawing_node.z;
        //"The Grid" process
        float lx = (mouseX-width/2);//Length
        float ly = (mouseY-height/2);
        float qx = floor(lx/grid);//Quocient
        float qy = floor(ly/grid);
        float fx = (lx%grid)/grid;//Grid-factor
        float fy = (ly%grid)/grid;
        if(fx < 0){
            fx = 1 + fx;
        }
        if(fy < 0){
            fy = 1 + fy;
        }
        if(fx < 0.15){
            lx = qx*grid;
        }  
        else if (fx > 0.85){
            lx = (qx+1)*grid;
        }
        if(fy < 0.15){
            ly = qy*grid;
        }  
        else if (fy > 0.85){
            ly = (qy+1)*grid;
        }
        if (orientation == "xy") {
            stroke(Color.yellow);
            strokeWeight(h(3));
            line(x0, y0, z0, x0+lx, y0+ly,z0+ 0);
            //components
            stroke(Color.white);
            strokeWeight(h(2));
            if (lx > h(50)){
                line(x0+h(20), y0, z0, x0+lx-h(20), y0,z0);
            } 
            else if (lx < -h(50)){
                line(x0-h(20), y0, z0, x0+lx+h(20), y0,z0);
            }    
            if (ly > h(50)){
                line(x0+lx, y0+h(20), z0, x0+lx, y0+ly-h(20),z0);
            } 
            else if (ly < -h(50)){
                line(x0+lx, y0-h(20), z0, x0+lx, y0+ly+h(20),z0);
            }  
            textSize(h(20));
            strokeWeight(h(4));
            point(x0+lx,y0,z0);
            fill(Color.white);
            pushMatrix();
            translate(x0+(lx)/2-textWidth(str((lx)/scale))/2, y0-h(5), z0);
            text(str((lx)/scale),0,0);
            popMatrix();
            pushMatrix();
            translate(x0+(lx)+h(10), y0-(-ly)/2+h(10), z0);
            text(str((-ly)/scale),0,0);
            popMatrix();
        } else if (orientation == "yz") {
            stroke(Color.yellow);
            strokeWeight(h(3));
            line(x0, y0, z0, x0+0,y0 -lx,z0-ly);
            //components
            stroke(Color.white);
            strokeWeight(h(2));
            if (-lx > h(50)){
                line(x0, y0+h(20), z0, x0, y0-lx-h(20),z0);
            } 
            else if (-lx < -h(50)){
                line(x0, y0-h(20), z0, x0, y0-lx+h(20),z0);
            }    
            if (-ly > h(50)){
                line(x0, y0-lx, z0+h(20), x0, y0-lx,z0-ly-h(20));
            } 
            else if (-ly< -h(50)){
                line(x0, y0-lx, z0-h(20), x0, y0-lx,z0-ly+h(20));
            }  
            textSize(h(20));
            strokeWeight(h(4));
            point(x0,y0-lx,z0);
            fill(Color.white);
            pushMatrix();
            translate(x0, y0+(-lx)/2+textWidth(str((lx)/scale))/2, z0+h(5));
            rotateX(-PI/2);
            rotateY(-3*PI/2);
            text(str((lx)/scale),0,0);
            popMatrix();
            pushMatrix();
            translate(x0, y0+(-lx)-h(10), z0+(-ly)/2-h(10));
            rotateX(-PI/2);
            rotateY(-3*PI/2);
            text(str((-ly)/scale),0,0);
            popMatrix();
        } else if (orientation == "xz") {
            stroke(Color.yellow);
            strokeWeight(h(3));
            line(x0, y0, z0, x0+lx, y0+0,z0-ly);
            //components
            stroke(Color.white);
            strokeWeight(h(2));
            if (lx > h(50)){
                line(x0+h(20), y0, z0, x0+lx-h(20), y0,z0);
            } 
            else if (lx < -h(50)){
                line(x0-h(20), y0, z0, x0+lx+h(20), y0,z0);
            }    
            if (-ly > h(50)){
                line(x0+lx, y0, z0+h(20), x0+lx, y0,z0-ly-h(20));
            } 
            else if (-ly < -h(50)){
                line(x0+lx, y0, z0-h(20), x0+lx, y0,z0-ly+h(20));
            }  
            textSize(h(20));
            strokeWeight(h(4));
            point(x0+lx,y0,z0);
            fill(Color.white);
            pushMatrix();
            translate(x0+(lx)/2-textWidth(str((lx)/scale))/2, y0, z0+h(5));
            rotateX(-PI/2);
            text(str((lx)/scale),0,0);
            popMatrix();
            pushMatrix();
            translate(x0+(lx)+h(10), y0, z0+(-ly)/2+h(10));
            rotateX(-PI/2);
            text(str((-ly)/scale),0,0);
            popMatrix();
        }
    }
    void initiate() {
        textSize(v(30));
        fill(Color.grey, 150+50*sin(millis()/1500.0));
        text("<< Click to Start >>", h(655), v(820));
        noStroke();
        translate(mouseX, mouseY);
        axis();
        noStroke();
        fill(Color.yellow);
        ellipse(0, 0, h(10), h(10));
        //sphere(h(10));
        fill(Color.yellow, 50);
        //sphere(h(30+10*sin(millis()/1000.0)));
        ellipse(0, 0, h(30+10*sin(millis()/1000.0)), h(30+10*sin(millis()/1000.0)));
        if (mousePressed) {
            mouse_pressed = true;
        }
        if(mouse_pressed && !mousePressed){
            mouse_pressed = false;
            Node node = new Node(0,0,0);
            Model.addNode(node);
            tx = mouseX;
            ty = mouseY;
            target_tx = width/2;
            target_ty = height/2;
            target_rx = PI/4;
            //target_ry = PI/3;
            target_rz = PI/4;
        }
    }
    void sweetMove() {
        float c = 1/10.0;
        tx = tx + (target_tx-tx)*c;
        ty = ty + (target_ty-ty)*c;
        tz = tz + (target_tz-tz)*c;
        rx = rx + (target_rx-rx)*c;
        ry = ry + (target_ry-ry)*c;
        rz = rz + (target_rz-rz)*c;
    }
    void frame(){
        sweetMove();
        translate(tx, ty, tz);
        rotateX(rx);
        rotateY(ry);
        rotateZ(rz);
    }
    void keyboard(){
        if(keyPressed){
            if (key == 't'){
                Interface.target(new Node(0,0,0));
            }
            if (key == 'm'){
                Interface.target_tz += 10;
            }
            if (key == 'n'){
                Interface.target_tz -= 10;
            }
            if (keyCode == RIGHT){
                Interface.target_tx += 10;
            }
            if (keyCode == LEFT){
                Interface.target_tx -= 10;
            }
            if (keyCode == UP){
                Interface.target_ty -= 10;
            }
            if (keyCode == DOWN){
                Interface.target_ty += 10;
            }
        }
    }
    void createBar(){
        float x_ = drawing_node.x;
        float y_ = drawing_node.y;
        float z_ = drawing_node.z;
        if (orientation == "xy") {
            x_ += mouseX - width/2;
            y_ += mouseY - height/2;
            z_ += 0;
        } else if (orientation == "yz") {
            x_ += 0;
            y_ += -mouseX+width/2;
            z_ += -mouseY+height/2;
        } else if (orientation == "xz") {
            x_ += mouseX-width/2;
            y_ += 0;
            z_ += -mouseY+height/2;
        }
        Node node_s = new Node(x_,y_,z_);
        int n = Model.nodeNumber(node_s);
        if (n == -1){
            Model.addNode(node_s);
        }
        if (Model.nodeNumber(node_s) != -1){ //There is a copy of this node in the model
            node_s = Model.node[Model.nodeNumber(node_s)];
        }
        target(node_s);
        Bar bar = new Bar(drawing_node,node_s);
        Model.addBar(bar);
    }
    void show3D() {
        background(0);
        mouse();
        keyboard();
        frame();
        if (mode == "initial"){
            initiate();
            if (Model.n_node != 0 ){
                mode = "visual";
            }
        }
        else if (mode == "visual"){
            axis();
            Model.showBars();
            Model.showNodes();
        }
        else if (mode == "drawing"){
            axis();
            Model.showBars();
            Model.showNodes();
            drawLine();
            if (!mousePressed){
                createBar();
                mode = "visual";
            }
        }
        else if (mode == "node_options"){
            axis();
            Model.showBars();
            Model.showNodes();
        }
        //else if (mode == "bar_options"){
            //barOptions();
        //}
    }
    void show2D(){
        if (mode == "node_options"){
            nodeOptions();
        }
        else if (mode == "bar_options"){
            barOptions();
        }
    }
    void mouse() {
        pushMatrix();
        translate(mouseX, mouseY);
        stroke(Color.pink);
        strokeWeight(1);
        line(-h(5), 0, h(5), 0);
        line(0, -h(5), 0, h(5));
        //strokeWeight(h(2));
        //fill(Color.pink);
        //text(str(mouseX)+","+str(mouseY),0,0);
        popMatrix();

    }
}

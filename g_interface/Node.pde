class Node {
    float x, y, z;
    float size = 0;
    boolean mouse_over;
    boolean mouse_pressed;
    boolean selecting_plan;
    boolean selected;
    boolean dragged;
    int last_pressed;

    Node(float x,float y,float z) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.mouse_over = false;
        this.mouse_pressed = false;
        this.selected = false;
        this.selecting_plan = false;
        this.dragged = false;
        this.last_pressed = 0;
    }
    void refreshState(){
        boolean right_clicked = false;
        boolean left_clicked = false;
        if (pow(screenX(this.x, this.y, this.z)-mouseX, 2)+pow(screenY(this.x, this.y, this.z)-mouseY, 2)<h(20)*h(20)) {
            this.mouse_over = true;
        } else {
            this.mouse_over = false;
        }
        if (!this.mouse_pressed && this.mouse_over && mousePressed) {
            this.mouse_pressed = true;
            this.last_pressed = millis();
        }
        if (this.mouse_pressed && !mousePressed) {
            this.mouse_pressed = false;
            if (millis()-this.last_pressed < 300){
                if( mouseButton == LEFT ){
                    left_clicked = true;
                }
                else if (mouseButton == RIGHT){
                    right_clicked = true;
                }
            }
        }
        if (mouse_pressed && millis()-this.last_pressed > 300){
            this.dragged = true;
        }
        if (right_clicked){
            if (this.selected) {
                this.selected = false;
            } else {
                this.selected = true;
            }
        }
        if (left_clicked){
            if (this.selecting_plan){
                this.selecting_plan = false;
            } else{
                this.selecting_plan = true;
            }
        }
    }
    void show() {
        refreshState();
        if (this.dragged) {
            Interface.startLine(this);
            this.dragged = false;
        }
        if (this.selecting_plan) {
            fill(Color.yellow);
            Interface.target(this);
            boolean r = Interface.selectPlan(this);
            if (r) {
                this.selecting_plan = false; 
            }
        } else {
            fill(Color.white);
        } 
        if (this.selected){
            Interface.startNodeOptions(this);
        }  
        pushMatrix();
        translate(this.x, this.y, this.z);
        noStroke();
        if (mouse_over){
            sphere(h(20+10*sin(millis()/1000.0)));
        }
        else{
            sphere(h(5));
        }
        popMatrix();
    }
}
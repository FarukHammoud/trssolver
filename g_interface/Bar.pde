class Bar {
	Node node_r;
	Node node_s;
	String type;
	float EI;
	float ES;

	Bar(Node node_r,Node node_s) {
  		this.node_r = node_r;
  		this.node_s = node_s;
  		this.type = "elastic";
  		this.EI = 1;
  		this.ES = 1;
  	}

  	void show() {
    	stroke(Color.grey);
    	strokeWeight(h(3));
    	line(this.node_r.x, this.node_r.y,	this.node_r.z, this.node_s.x, this.node_s.y, this.node_s.z);
  	}
}

//DEFINES
int MAP_MAX = 1000;

//Funções Anexas
void definitions() {
	textureMode(NORMAL);
	textMode(SHAPE);
	noCursor();
}

//Funções Main
void setup() {
	fullScreen(P3D);
	//size(800,450,P3D);
	println(width,height);
	poli = loadImage("poli.png");
	definitions();
}
void draw() {
	hint(ENABLE_DEPTH_TEST);
	
	Interface.show3D();

	camera(); 
    hint(DISABLE_DEPTH_TEST);

    Interface.show2D();
}


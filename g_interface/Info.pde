class Info {
  String message = "";
  void show() {
    fill(Color.blue);
    text(message, h(1500), v(100));
  }
}
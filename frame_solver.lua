local matrix = require "matrix"
PolarVector = {}
function PolarVector:new(x,y,gama)
	local n_vector = setmetatable({},{__index = PolarVector})
	n_vector.x = x
	n_vector.y = y
	n_vector.gama = gama 
	return n_vector
end
function t(wik,wjk)
	if math.abs(wik) > math.abs(wjk) then
		return -wjk/wik
	else
		return -wik/wjk
	end
end
function s(wik,wjk)
	if math.abs(wik) > math.abs(wjk) then
		return c(wik,wjk)*t(wik,wjk)
	else
		return 1.0/math.sqrt(1.0+t(wik,wjk)^2)
	end
end
function c(wik,wjk)
	if math.abs(wik) > math.abs(wjk) then
		return 1.0/math.sqrt(1.0+t(wik,wjk)^2)
	else
		return s(wik,wjk)*t(wik,wjk)
	end
end
function givens(W,i,j,c,s)
	local n = #W
	local m = #W[1]
	for r = 1, m do
		local aux = c * W[i][r] - s*W[j][r]
		W[j][r] = s * W[i][r] + c * W[j][r]
		W[i][r] = aux
	end
end
function solve(W,b)
	local n = #W
	local m = #W[1]
	x = matrix:new(m,1,0)
	for k = 1, m do
		for j = n, k+1, -1 do
			local i = j - 1
			if W[j][k] ~= 0 then
				local c_ = c(W[i][k],W[j][k])
				local s_ = s(W[i][k],W[j][k])
				givens(W,i,j,c_,s_)
				givens(b,i,j,c_,s_)
			end
		end
	end
	for k = m, 1, -1 do
		local summation = 0
		for j = k+1, m do
			summation = summation + W[k][j]*x[j][1]
		end
		x[k][1] = (b[k][1] - summation)/W[k][k]
	end
	return x
end
Node = {
}
function Node:new(x,y,frame,ID)
	local n_node = setmetatable({},{__index = Node})
	n_node.x = x
	n_node.y = y
	if frame == nil then
		n_node.frame = 0
	else
		n_node.frame = frame
	end
	n_node.ID = ID
	n_node.load = PolarVector:new(false,false,false)
	n_node.displacement = PolarVector:new(false,false,false)
	return n_node
end
Beam = {
}
function Beam:new(node_r,node_s,ES,EI)
	local n_beam = setmetatable({},{__index = Beam})
	n_beam.node_r = node_r
	n_beam.node_s = node_s
	n_beam.ES = ES
	n_beam.EI = EI
	return n_beam
end
function Beam:length()
	return math.sqrt(math.pow(self.node_r.x-self.node_s.x,2)+math.pow(self.node_r.y-self.node_s.y,2))
end
function Beam:incoming()
	local dx = self.node_s.x - self.node_r.x
	local dy = self.node_s.y - self.node_r.y
	if dx > 0 then
		return math.atan(dy/dx)
	elseif dx < 0 then
		return math.pi + math.atan(dy/dx)
	else 
		if dy >= 0 then
			return math.pi/2
		else
			return -math.pi/2
		end
	end
end
function Beam:T()
	T = matrix:new(6,6,0)
	T[1][1] = math.cos(self:incoming()-self.node_r.frame)
	T[1][2] = math.sin(self:incoming()-self.node_r.frame)
	T[2][1] = -math.sin(self:incoming()-self.node_r.frame)
	T[2][2] = math.cos(self:incoming()-self.node_r.frame)
	T[3][3] = 1
	T[4][4] = math.cos(self:incoming()-self.node_s.frame)
	T[4][5] = math.sin(self:incoming()-self.node_s.frame)
	T[5][4] = -math.sin(self:incoming()-self.node_s.frame)
	T[5][5] = math.cos(self:incoming()-self.node_s.frame)
	T[6][6] = 1
	return T
end
function Beam:k()
	local T_inv = matrix.invert(self:T())
	local local_kT = matrix.mul(self:local_k(),self:T())
	return matrix.mul(T_inv,local_kT)
end
function Beam:local_k()
	local K = matrix:new(6,6,0)
	K[2][2] = 12*self.EI
	K[2][5] = -12*self.EI
	K[5][2] = -12*self.EI
	K[5][5] = 12*self.EI
	k = matrix.mulnum(K,1/self:length())
	K[2][3] = 6*self.EI
	K[2][6] = 6*self.EI
	K[5][3] = -6*self.EI
	K[5][6] = -6*self.EI
	K[3][2] = 6*self.EI
	K[3][5] = -6*self.EI
	K[6][2] = 6*self.EI
	K[6][5] = -6*self.EI
	k = matrix.mulnum(K,1/self:length())
	K[3][3] = 4*self.EI
	K[3][6] = 2*self.EI
	K[6][3] = 2*self.EI
	K[6][6] = 4*self.EI
	K[1][1] = self.ES
	K[1][3] = -self.ES
	K[3][1] = -self.ES
	K[3][3] = self.ES
	K = matrix.mulnum(K,1/self:length())
	return K
end
Frame = {
}
function Frame:new(path)
	local n_frame = setmetatable({},{__index = Frame})
	n_frame.nodes = {}
	n_frame.beams = {}
	n_frame:load_file(path)
	n_frame:check()
	return n_frame
end
function Frame:L(IDr,IDs)
	local L = matrix:new(6,3*self.n_nodes,0)
	L[1][3*IDr-2] = 1
	L[2][3*IDr-1] = 1
	L[3][3*IDr] = 1
	L[4][3*IDs-2] = 1
	L[5][3*IDs-1] = 1
	L[6][3*IDs] = 1
	return L
end
function Frame:K()
	local K = matrix:new(3*self.n_nodes,3*self.n_nodes,0)
	for i,beam in ipairs(self.beams) do
		L = self:L(beam.node_r.ID,beam.node_s.ID)
		L_t = matrix.transpose(L)
		K = matrix.add(K,matrix.mul(L_t,matrix.mul(beam:k(),L))) -- SIGNAL beam - frame HERE
	end
	return K
end
function Frame:set_U(i,value)
	if i%3 == 1 then
		self.nodes[math.ceil(i/3)].displacement.x = value
	elseif i%3 == 2 then
		self.nodes[math.ceil(i/3)].displacement.y = value
	else
		self.nodes[math.ceil(i/3)].displacement.gama = value
	end
end
function Frame:set_F(i,value)
	if i%3 == 1 then
		self.nodes[math.ceil(i/3)].load.x = value
	elseif i%3 == 2 then
		self.nodes[math.ceil(i/3)].load.y = value
	else
		self.nodes[math.ceil(i/3)].load.gama = value
	end
end
function Frame:U(i)
	if i%3 == 1 then
		return self.nodes[math.ceil(i/3)].displacement.x
	elseif i%3 == 2 then
		return self.nodes[math.ceil(i/3)].displacement.y
	else
		return self.nodes[math.ceil(i/3)].displacement.gama
	end
end
function Frame:F(i)
	if i%3 == 1 then
		return self.nodes[math.ceil(i/3)].load.x
	elseif i%3 == 2 then
		return self.nodes[math.ceil(i/3)].load.y
	else
		return self.nodes[math.ceil(i/3)].load.gama
	end
end
function Frame:order()
	order = {}

	for i,node in ipairs(self.nodes) do
		if node.load.x then
			table.insert(order,3*i-2)
		end
		if node.load.y then
			table.insert(order,3*i-1)
		end
		if node.load.gama then
			table.insert(order,3*i)
		end
	end
	cut = #order
	for i,node in ipairs(self.nodes) do
		if not node.load.x then
			table.insert(order,3*i-2)
		end
		if not node.load.y then
			table.insert(order,3*i-1)
		end
		if not node.load.gama then
			table.insert(order,3*i)
		end
	end

	return order, cut
end
function Frame:U_beta()
	o, cut = self:order()
	U_beta = {}
	for i = cut+1, 3*self.n_nodes do
		table.insert(U_beta,{self:U(o[i])})
	end
	return U_beta
end
function Frame:F_alpha()
	o, cut = self:order()
	F_alpha = {}
	for i = 1, cut do
		table.insert(F_alpha,{self:F(o[i])})
	end
	return F_alpha
end
function Frame:K_sub_matrices()

	local K = self:K()
	local o, cut = self:order()
	local ordenated_K = matrix:new(3*self.n_nodes,3*self.n_nodes,0)
	for i = 1, 3*self.n_nodes do
		for j = 1, 3*self.n_nodes do
			ordenated_K[i][j] = K[o[i]][o[j]] 
		end
	end

	A = matrix.subm(ordenated_K,1,1,cut,cut)
	B = matrix.subm(ordenated_K,1,cut+1,cut,3*self.n_nodes)
	C = matrix.subm(ordenated_K,cut+1,1,3*self.n_nodes,cut)
	D = matrix.subm(ordenated_K,cut+1,cut+1,3*self.n_nodes,3*self.n_nodes)

	return A,B,C,D
end

function Frame:solve()

	A,B,C,D = self:K_sub_matrices()
	U_alpha = solve(A,matrix.mul(B,self:U_beta())-self:F_alpha())
	F_beta = matrix.mul(C,U_alpha)+matrix.mul(D,self:U_beta())
	
	o,cut = self:order()
	for i = 1, cut do
		self:set_U(o[i],-U_alpha[i][1])
	end
	for i = cut+1,3*self.n_nodes do
		self:set_F(o[i],-F_beta[i-cut][1])
	end
	print('[SOLVED] The frame is solved.')
end
function Frame:check()
	local target = 3*self.n_nodes
	for i,node in ipairs(self.nodes) do
		if not node.displacement.x then
			if not node.load.x then
				print('[UNDERDEFINED] Both disp_x and load_x arent settled for node ',i)
			else
				target = target - 1 
			end
		elseif not node.load.x then
			target = target -1
		else
			print('[OVERDEFINED] Both disp_x and load_x are settled for node ',i)
		end
		if not node.displacement.y then
			if not node.load.y then
				print('[UNDERDEFINED] Both disp_y and load_y arent settled for node ',i)
			else
				target = target - 1 
			end
		elseif not node.load.x then
			target = target -1
		else
			print('[OVERDEFINED] Both disp_y and load_y are settled for node ',i)
		end
		if not node.displacement.gama then
			if not node.load.gama then
				print('[UNDERDEFINED] Both disp_gama and load_gama arent settled for node ',i)
			else
				target = target - 1 
			end
		elseif not node.load.gama then
			target = target -1
		else
			print('[OVERDEFINED] Both disp_gama and load_gama are settled for node ',i)
		end
	end
	if target == 0 then
		print('[CHECKED] The frame can be solved.')
	end
end
function Frame:load_file(path)
    local lines = {}
    for line in io.lines(path) do
    	local codes = {}
    	for item in string.gmatch(line, "%S+") do
      		table.insert(codes,item)
      	end
      	table.insert(lines,codes)
    end
    for i,line in ipairs(lines) do
    	if line[1] == 'N' then
    		ID = #self.nodes+1
    		self.nodes[ID] = Node:new(tonumber(line[2]),tonumber(line[3]),tonumber(line[4]),ID)
    	elseif line[1] == 'B' then
    		self.beams[#self.beams+1] = Beam:new(self.nodes[tonumber(line[2])],self.nodes[tonumber(line[3])],tonumber(line[4]),tonumber(line[5]))
    	elseif line[1] == 'F' then
    		if line[3] == 'X' then
    			self.nodes[tonumber(line[2])].load.x = tonumber(line[4])
    		elseif line[3] == 'Y' then
    			self.nodes[tonumber(line[2])].load.y = tonumber(line[4])
    		elseif line[3] == 'G' then
    			self.nodes[tonumber(line[2])].load.gama = tonumber(line[4])
    		end
    	elseif line[1] == 'U' then
    		if line[3] == 'X' then
    			self.nodes[tonumber(line[2])].displacement.x = tonumber(line[4])
    		elseif line[3] == 'Y' then
    			self.nodes[tonumber(line[2])].displacement.y = tonumber(line[4])	
    		elseif line[3] == 'G' then
    			self.nodes[tonumber(line[2])].displacement.gama = tonumber(line[4])	
    		end
    	end
    end
    self.n_nodes = ID
end
--frame = Frame:new('frames/1.trs')
--Frame:solve()



